package com.braingarden.comparator.rest;

import com.braingarden.comparator.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ComparatorRestControllerTest {

    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TripletDAO tripletDAO;

    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void testFileManager(){

    }

    @Test
    public void testStaticHtml() {
        try {
            MvcResult r = mvc.perform(MockMvcRequestBuilders
                    .get("/static/test.html")
                    .accept(MediaType.TEXT_HTML))
                    .andReturn();
            assertThat(r.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
            assertThat(r.getResponse().getContentAsString()).contains("<title>test");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStaticImage() {
        MvcResult r = null;
        try {
            r = mvc.perform(MockMvcRequestBuilders
                    .get("/static/test_1.jpg")
                    .accept(MediaType.IMAGE_JPEG))
                    .andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertThat(r.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
            assertThat(r.getResponse().getContentType()).isEqualTo(MediaType.IMAGE_JPEG.toString());
    }

    @Test
    public void getUnlabeledTriplets() throws Exception {
        int nTriplets = 13;

        UnlabeledRequestModel requestModel = new UnlabeledRequestModel("testuser", nTriplets);
        String request = objectMapper.writeValueAsString(requestModel);
        MvcResult r = mvc.perform(MockMvcRequestBuilders
                .get("/api")
                .content(request)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertThat(r.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(r.getResponse().getContentType().contains(MediaType.APPLICATION_JSON.toString()));
        String respose = r.getResponse().getContentAsString();
        UnlabeledResponseModel responseModel = objectMapper.readValue(respose, UnlabeledResponseModel.class);
        assertThat(responseModel.getUnlabeledData().length == nTriplets);
        Arrays.stream(responseModel.getUnlabeledData())
                .forEach(x -> {
                    assertThat(x.length == 3);
                    assertThat(ArrayUtils.contains(x, "test_1.jpg"));
                    assertThat(ArrayUtils.contains(x, "test_2.jpg"));
                    assertThat(ArrayUtils.contains(x, "test_3.jpg"));
                });
    }

    @Test
    public void postLabeledTriplets() throws Exception {
        LabeledRequestModel requestModel = new LabeledRequestModel("testuser",
                new String[][]{{"dao_test_1.jpg", "dao_test_2.jpg", "dao_test_3.jpg"},
                        {"dao_test_4.jpg", "dao_test_5.jpg", "dao_test_6.jpg"}}, false);
        String request = objectMapper.writeValueAsString(requestModel);
        MvcResult r = mvc.perform(MockMvcRequestBuilders
                .post("/api")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(request))
                .andReturn();
        String response = r.getResponse().getContentAsString();
        assertThat(r.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(r.getResponse().getContentType().contains(MediaType.APPLICATION_JSON.toString()));
        List<Triplet> triplets = new ArrayList<>();
        LabeledResponseModel responseModel = objectMapper.readValue(response, LabeledResponseModel.class);
        Arrays.stream(responseModel.getIds())
                .forEach(x -> triplets.add(tripletDAO.findOne(x)));
        for (int i = 0; i < triplets.size(); i++) {
            assertThat(Arrays.equals(requestModel.getLabeledData()[i], triplets.get(i).toArray()));
        }
    }
}
