package com.braingarden.comparator.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/*
TODO caching? file lookup cache?
 */
public class StaticFileManager {
    private Logger logger = LogManager.getLogger(this.getClass());

    @Value("${static.dir}")
    private String staticFilesPath;



    private final String[] extensions = new String[] { "jpg", "jpeg" };

    public String[] getRandomTriplet() throws Exception {
        URL staticDir = null;
        try {
            staticDir = (staticFilesPath.substring(0, 1).equals("/"))
                    ? new URL(String.format("file:%s", staticFilesPath))
                    : StaticFileManager.class.getClassLoader().getResource(staticFilesPath);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        logger.info(String.format("Attempt to read static dir: %s",
                staticDir));
        List<String> files = FileUtils.listFiles(
                new File(staticDir.getPath()),
                extensions,
                false)
                .stream().map(File::getName).collect(Collectors.toList());
        if (files.size() < 3) throw new Exception("Not enough static files: " + files.toString());
        Random random = new Random();
        String[] randomFiles = new String[3];
        int i = 0;
        while (i < 3) {
            String rFile = files.get(random.nextInt(files.size()));
            if (ArrayUtils.contains(randomFiles, rFile)) continue;
            randomFiles[i] = rFile;
            i++;
        }
        return randomFiles;
    }
}
