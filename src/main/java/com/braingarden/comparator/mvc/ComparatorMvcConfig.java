package com.braingarden.comparator.mvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class ComparatorMvcConfig extends WebMvcConfigurerAdapter {

    @Value("${static.dir}")
    private String staticDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/",
                        (staticDir.substring(0, 1).equals("/"))
                        ? String.format("file:%s", staticDir)
                        : String.format("classpath:/%s", staticDir));
    }
}