package com.braingarden.comparator.rest;

import com.braingarden.comparator.models.*;
import com.braingarden.comparator.util.StaticFileManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/*
TODO log requests
 */
@RestController
@RequestMapping(value = "/api", produces = "application/json")
public class ComparatorRestController {
    private Logger logger = LogManager.getLogger(this.getClass());

    private final TripletDAO tripletDAO;

    private final StaticFileManager fileManager;

    @Autowired
    public ComparatorRestController(TripletDAO tripletDAO, StaticFileManager fileManager) {
        this.tripletDAO = tripletDAO;
        this.fileManager = fileManager;
    }


    @RequestMapping( method = RequestMethod.GET)
    public UnlabeledResponseModel getUnlabeledTriplets(UnlabeledRequestModel requestModel,
                                                       HttpServletRequest request, HttpServletResponse response){
        // TODO log request
        // TODO implement checking and selection
        logger.info(String.format("Got unlabeled triplets request: %s", requestModel.toString()));
        List<CompletableFuture<String[]>> futures = new ArrayList<>();
        for (int i = 0; i < requestModel.getnTriplets(); i++) {
            futures.add(CompletableFuture.supplyAsync(() -> {
                try {
                    return fileManager.getRandomTriplet();
                } catch (Exception e) {
                    logger.error(e);
                }
                return null;
            }));
        }
        List<String[]> result = futures.stream()
                .peek(x -> logger.info(x))
                .filter(x -> Objects.nonNull(x))
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
        return new UnlabeledResponseModel(result.toArray(new String[result.size()][]));
    }

    @RequestMapping(method=RequestMethod.POST)
    public LabeledResponseModel postLabeledTriplets(@RequestBody LabeledRequestModel requestModel,
                                                    HttpServletRequest request, HttpServletResponse response){
        // TODO log request
        // TODO implement checking and selection
        logger.info(String.format("Got labeled triplets request: %s", requestModel.toString()));
        List<String[]> triplets = Arrays.asList(requestModel.getLabeledData());
        List<Long> result = triplets.stream()
                .map(x ->
                        CompletableFuture.supplyAsync(() -> {
                            Triplet tt = null;
                            try {
                                tt = new Triplet(x, requestModel.getHasSimilarity());
                                tripletDAO.save(tt);
                                logger.info(String.format("Triplet saved through DAO: %s", tt.toString()));
                            }
                            catch (Exception e) {
                                logger.error("Error saving triplet", e);
                                response.setStatus( HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                            }
                            return tt;
                        }))
                .map(CompletableFuture::join)
                .map(Triplet::getId)
                .collect(Collectors.toList());

        return new LabeledResponseModel(result.toArray(new Long[result.size()]));
    }


}
