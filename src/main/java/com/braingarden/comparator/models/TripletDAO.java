package com.braingarden.comparator.models;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TripletDAO extends CrudRepository<Triplet, Long> {

}
