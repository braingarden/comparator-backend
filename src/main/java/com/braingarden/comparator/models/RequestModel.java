package com.braingarden.comparator.models;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public abstract class RequestModel {

    @JsonProperty
    protected String userId = "anonymous";

    @JsonGetter
    public String getUserId() {
        return userId;
    }

    @JsonSetter
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
