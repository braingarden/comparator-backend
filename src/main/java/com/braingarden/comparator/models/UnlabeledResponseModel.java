package com.braingarden.comparator.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class UnlabeledResponseModel {

    private String[][] unlabeledData;

    @JsonGetter
    public String[][] getUnlabeledData() {
        return unlabeledData;
    }

    @JsonSetter
    public void setUnlabeledData(String[][] unlabeledData) {
        this.unlabeledData = unlabeledData;
    }

    @JsonCreator
    public UnlabeledResponseModel(@JsonProperty("unlabeledData") String[][] unlabeledData) {
        this.unlabeledData = unlabeledData;
    }
}
