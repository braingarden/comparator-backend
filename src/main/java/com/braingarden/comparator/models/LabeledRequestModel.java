package com.braingarden.comparator.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class LabeledRequestModel extends RequestModel {

    private String[][] labeledData;
    private boolean hasSimilarity;

    public LabeledRequestModel() {
    }

    @JsonCreator
    public LabeledRequestModel(@JsonProperty("userId") String userId,
                               @JsonProperty("labeledData") String[][] labeledData,
                               @JsonProperty("hasSimilarity") boolean hasSimilarity) {
        this.userId = userId;
        this.labeledData = labeledData;
        this.hasSimilarity = hasSimilarity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LabeledRequestModel that = (LabeledRequestModel) o;

        return hasSimilarity == that.hasSimilarity && Arrays.deepEquals(labeledData, that.labeledData);
    }

    public boolean getHasSimilarity() {
        return hasSimilarity;
    }

    public void setHasSimilarity(boolean hasSimilarity) {
        this.hasSimilarity = hasSimilarity;
    }

    public String[][] getLabeledData() {
        return labeledData;
    }

    public void setLabeledData(String[][] labeledData) {
        this.labeledData = labeledData;
    }

    @Override
    public String toString() {
        return "LabeledRequestModel{" +
                "labeledData=" + Arrays.toString(labeledData) +
                ", hasSimilarity=" + hasSimilarity +
                ", userId='" + userId + '\'' +
                '}';
    }
}
