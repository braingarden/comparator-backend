package com.braingarden.comparator.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UnlabeledRequestModel extends RequestModel {

    private int nTriplets = 10;



    public UnlabeledRequestModel() {
    }

    @JsonCreator
    public UnlabeledRequestModel(@JsonProperty("userId") String userId, @JsonProperty("nTriplets") int nTriplets) {
        this.userId = userId;
        this.nTriplets = nTriplets;
    }

    @Override
    public String toString() {
        return "UnlabeledRequestModel{" +
                "nTriplets=" + nTriplets +
                ", userId='" + userId + '\'' +
                '}';
    }

    public int getnTriplets() {
        return nTriplets;
    }

    public void setnTriplets(int nTriplets) {
        this.nTriplets = nTriplets;
    }
}
