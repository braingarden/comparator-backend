package com.braingarden.comparator.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class LabeledResponseModel {
    private long[] ids;

    public LabeledResponseModel(Long[] ids) {
        long[] primitiveIds = new long[ids.length];
        for (int i = 0; i < ids.length; i++) {
            primitiveIds[i] = ids[i];
        }
        this.ids = primitiveIds;
    }

    @JsonCreator
    public LabeledResponseModel(@JsonProperty("ids") long[] ids) {
        this.ids = ids;
    }

    @JsonGetter
    public long[] getIds() {
        return ids;
    }

    @JsonSetter
    public void setIds(long[] ids) {
        this.ids = ids;
    }


}
