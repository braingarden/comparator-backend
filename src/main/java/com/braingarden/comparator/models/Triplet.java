package com.braingarden.comparator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name= "labeled_data")
public class Triplet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @NotNull
    @Column(name = "target")
    private String target;

    @NotNull
    @Column(name = "first")
    private String first;

    @NotNull
    @Column(name = "second")
    private String second;

    @NotNull
    @Column(name = "has_similarity")
    private boolean hasSimilarity;

    public Triplet() {
    }

    public Triplet(long id) {
        this.id = id;
    }

    public Triplet(String[] items, boolean hasSimilarity){
        this(items[0], items[1], items[2], hasSimilarity);
    }

    public Triplet(String target, String first, String second, boolean hasSimilarity) {
        this.target = target;
        this.first = first;
        this.second = second;
        this.hasSimilarity = hasSimilarity;
    }

    public String[] toArray(){
        return new String[]{target, first, second};
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @JsonIgnore
    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    @JsonIgnore
    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    @JsonIgnore
    public boolean getHasSimilarity() {
        return hasSimilarity;
    }

    public void setHasSimilarity(boolean hasSimilarity) {
        this.hasSimilarity = hasSimilarity;
    }

    @Override
    public String toString() {
        return "Triplet{" +
                "id=" + id +
                ", target='" + target + '\'' +
                ", first='" + first + '\'' +
                ", second='" + second + '\'' +
                ", hasSimilarity=" + hasSimilarity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triplet triplet = (Triplet) o;

        if (id != triplet.id) return false;
        if (hasSimilarity != triplet.hasSimilarity) return false;
        if (!target.equals(triplet.target)) return false;
        if (!first.equals(triplet.first)) return false;
        return second.equals(triplet.second);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + target.hashCode();
        result = 31 * result + first.hashCode();
        result = 31 * result + second.hashCode();
        result = 31 * result + (hasSimilarity ? 1 : 0);
        return result;
    }
}
