FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y -qq software-properties-common curl

# JAVA

RUN \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections && \
  apt-get install -y oracle-java9-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk9-installer

ENV JAVA_HOME /usr/lib/jvm/java-9-oracle

# MAVEN
ARG MAVEN_VERSION=3.3.9
#ARG SHA=5b4c117854921b527ab6190615f9435da730ba05
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
#  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn \
  && mkdir ~/.m2

ENV MAVEN_HOME /usr/share/maven
#ENV MAVEN_CONFIG ~/.m2

ENV LOG_LEVEL INFO
ENV STATIC_DIR /static
ENV POSTGRES_HOST postgres:5432
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres

RUN mkdir /comparator
COPY . /comparator
WORKDIR /comparator

RUN mvn -N io.takari:maven:wrapper
RUN mkdir /static
RUN chmod +x ./mvnw
RUN mvn clean install -DskipTests
EXPOSE 8080
CMD java -jar ./target/comparator.jar
