

# Backend server

## REST API


### Get unlabeled image ids

Get list of unlabeled ids

**URL** : `/api`

**Method** : `GET`

**Auth required** : NO

**Permissions required** : None

**Data params**

```json
{
    "userId": "String",
    "nTriplets": "int"
}
```

**Data constraints**

```json
{
    "userId": "[1 to 30 chars]",
    "nTriplets": "[1 < x < 100]"
}
```

**Default data values**

```json
{
    "userId": "anonymous",
    "nTriplets": 10
}
```

**Data examples**


```json
{
  "userId": "testuser",
  "nTriplets": 13
}
```

#### Success Response

**Code** : `200 OK`

**Content example**

Triplet contains three image id's: `[target.jpg, comparable_1.jpg, comparable_2.jpg]`

This request body will return list of image ids triplets `[y_1.jpg, x_1.jpg, x_2.jpg], [y_2.jpg, x_2.jpg, x_3.jpg]`
 
```json
{
  "unlabeledData": [["test_1.jpg","test_3.jpg","test_2.jpg"],["test_1.jpg","test_3.jpg","test_2.jpg"]]
}
```

#### Notes

* Any name in userId field is appliable, used only for debugging


### Get image

Get list of unlabeled ids

**URL** : `/static/:id`

**Method** : `GET`

**URL params** : 

`id: String` requested image id

**Data examples**


```json
{
    "id": "test.jpg"
}
```


#### Success Response

**Code** : `200 OK`

**Content example**

Return requested image


### Post labeled items

Post list of labeled ordered ids

**URL** : `/api`

**Method** : `POST`

**Auth required** : NO

**Permissions required** : None

**Data params**
One triplet contains three image id's: `[target.jpg, comparable_1.jpg, comparable_2.jpg]`

```json
{   
    "userId": "String",
    "labeledData": "[[String, String, String]]"
}
```

**Data constraints**

```json
{   
    "userId": "[1 to 30 chars]",
    "labeledData": "[1 to 100 of [3]]"
}
```

**Default data**

```json
{
    "userId": "anonymous"
}
```

**Data examples**


```json
{
  "userId": "testuser",
  "labeledData": [["dao_test_1.jpg","dao_test_2.jpg","dao_test_3.jpg"],["dao_test_4.jpg","dao_test_5.jpg","dao_test_6.jpg"]],
  "hasSimilarity": true
}
```



#### Success Response

**Code** : `200 OK`

**Content example**

Returns triplets db ids

```json
{
    "ids": [1, 2, 3]
}
```